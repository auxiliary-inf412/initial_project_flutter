import 'package:flutter/material.dart';
import 'package:initial_project_flutter/src/pages/login_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text("My Home Page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Hola este es mi menu"),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage(
                              parametro: "Hola vengo del home page")));
                },
                child: const Text("Login Page")),
            ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "ejemplo2_page");
                },
                child: const Text("Redirigir a la vista ejemplo 2")),
            ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "maps_page");
                },
                child: const Text("Redirigir a la vista de Mapas")),
            ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "camera_page");
                },
                child: const Text("Acceso a Cámara o Galería"))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, 'item_form_page');
        },
        child: const Icon(Icons.book),
      ),
    );
  }
}
