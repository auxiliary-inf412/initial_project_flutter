import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:initial_project_flutter/src/models/item.dart';

class ItemListPage extends StatelessWidget {
  const ItemListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Lista Item"),
        ),
        body: FutureBuilder<List<Item>>(
          initialData: [],
          future: _getItems(),
          builder: (context, snapshot) => _drawItems(context, snapshot),
        ));
  }

  Future<List<Item>> _getItems() async {
    Dio dio = Dio();
    var response = await dio.get("http://192.168.100.4:8000/api/item_manage");
    if (response.statusCode == 200) {
      List<dynamic> responseData = response.data;
      List<Item> items = [];
      for (dynamic item in responseData) {
        items.add(Item.fromJson(item));
      }
      return items;
    } else {
      print("ERROR!!!!");
      return [];
    }
  }

  _drawItems(BuildContext context, AsyncSnapshot<List<Item>> snapshot) {
    if (snapshot.hasData) {
      List<Item> items = snapshot.data!;
      return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(items[index].description),
            subtitle: Text(items[index].price),
            leading: Icon(Icons.emoji_objects_sharp),
          );
        },
      );
    } else {
      return Text("No hay Items que mostrar");
    }
  }
}
