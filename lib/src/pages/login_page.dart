import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  final String parametro;
  const LoginPage({Key? key, required this.parametro}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: Center(
        child: Text("Login ${parametro}"),
      ),
    );
  }
}
