import 'package:flutter/material.dart';

class Ejemplo2Page extends StatelessWidget {
  const Ejemplo2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ejemplo 2 Page"),
        centerTitle: true,
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.notifications)), IconButton(onPressed: () {}, icon: Icon(Icons.alarm))],
      ),
      body: ListView(
        children: [
          Card(
            child: ListTile(
              leading: Icon(Icons.numbers),
              title: Text("Item 1"),
              subtitle: Text("Subtitle of item 1"),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(Icons.numbers),
              title: Text("Item 2"),
              subtitle: Text("Subtitle of item 2"),
              trailing: Icon(Icons.accessibility_new),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(Icons.numbers),
              title: Text("Item 3"),
              subtitle: Text("Subtitle of item 3"),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(Icons.numbers),
              title: Text("Item 4"),
              subtitle: Text("Subtitle of item 4"),
            ),
          ),
          ListTile(
            leading: Icon(Icons.numbers),
            title: Text("Item 5"),
            subtitle: Text("Subtitle of item 5"),
          ),
          ListTile(
            leading: Icon(Icons.numbers),
            title: Text("Item 6"),
            subtitle: Text("Subtitle of item 6"),
          )
        ],
      ),
    );
  }
}
