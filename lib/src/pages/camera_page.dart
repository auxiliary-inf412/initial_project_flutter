import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({Key? key}) : super(key: key);

  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  XFile? _image;
  final ImagePicker _imagePicker = ImagePicker();

  void pickImage(ImageSource source) async {
    XFile? imagenCapturada = await _imagePicker.pickImage(source: source);
    if (imagenCapturada != null) {
      setState(() {
        _image = imagenCapturada;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Camera Page"),
        actions: [
          IconButton(
              onPressed: () {
                pickImage(ImageSource.camera);
              },
              icon: Icon(Icons.camera)),
          IconButton(
              onPressed: () {
                pickImage(ImageSource.gallery);
              },
              icon: Icon(Icons.image))
        ],
      ),
      body: Center(
          child: _image == null
              ? Text("No tengo una imagen")
              : Image.file(File(_image!.path))),
    );
  }
}
