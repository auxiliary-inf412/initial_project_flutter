import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:initial_project_flutter/src/models/item.dart';

class ItemFormPage extends StatefulWidget {
  const ItemFormPage({Key? key}) : super(key: key);

  @override
  State<ItemFormPage> createState() => _ItemFormPageState();
}

class _ItemFormPageState extends State<ItemFormPage> {
  String description = "";
  bool status = false;
  double price = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Registrar Item ")),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Formulario de Registro"),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextField(
              keyboardType: TextInputType.text,
              onChanged: (value) {
                setState(() {
                  description = value;
                });
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Estado: "),
              Switch(
                value: status,
                onChanged: (value) {
                  setState(() {
                    status = value;
                  });
                },
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextField(
              keyboardType: TextInputType.number,
              onChanged: (value) {
                setState(() {
                  price = double.parse(value);
                });
              },
            ),
          ),
          ElevatedButton(onPressed: _registrarItem, child: Text("Registrar")),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "item_list_page");
              },
              child: Text("Listar Items"))
        ],
      ),
    );
  }

  void _registrarItem() async {
    try {
      Dio dio = Dio();
      Item item = Item(
          description: description, status: status, price: price.toString());
      var response = await dio.post("http://192.168.100.4:8000/api/item_manage",
          data: item.toJson());
      if (response.statusCode == 200) {
        print("Item Guardado correctamente");
      } else {
        print("El item no se guardo correctamente");
      }
    } catch (e) {
      print("Hubo error en metodo _registrarItem ${e.toString()}");
    }
  }
}
