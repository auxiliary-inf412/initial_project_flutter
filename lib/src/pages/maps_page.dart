import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

class MapsPage extends StatefulWidget {
  const MapsPage({Key? key}) : super(key: key);

  @override
  State<MapsPage> createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  List<Marker> markers = [];

  /// TAREA: ARREGLAR ESTE ALGORITMO (ARREGLAR MEJOR LOS PERMISOS)
  Future<Position> _determinePosition() async {
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Map Page"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body: FlutterMap(
        options: MapOptions(
          center: LatLng(-17.783287, -63.182044),
          zoom: 12,
          onTap: (tapPosition, point) {
            print("POSITION OBTAINED: ${point.latitude},${point.longitude}");
            setState(() {
              markers.add(Marker(
                  point: LatLng(point.latitude, point.longitude),
                  builder: (context) => const Icon(Icons.location_on,
                      color: Colors.red, size: 35)));
            });
          },
        ),
        children: [
          TileLayer(
            urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
          ),
          MarkerLayer(
            markers: markers,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Position currentPosition = await _determinePosition();
            setState(() {
              markers.add(Marker(
                  point: LatLng(
                      currentPosition.latitude, currentPosition.longitude),
                  builder: (context) => const Icon(Icons.location_on,
                      color: Colors.deepPurple, size: 35)));
            });
            print(
                "POS: ${currentPosition.latitude}, ${currentPosition.longitude}");
          },
          child: Icon(Icons.location_city)),
    );
  }
}
