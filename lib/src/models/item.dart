import 'dart:convert';

Item itemFromJson(String str) => Item.fromJson(json.decode(str));

String itemToJson(Item data) => json.encode(data.toJson());

class Item {
  /// attributes
  final int? id;
  final String description;
  final bool status;
  final String price;
  final String? createdAt;
  final String? updatedAt;

  /// constructor
  Item({
    this.id,
    required this.description,
    required this.status,
    required this.price,
    this.createdAt,
    this.updatedAt,
  });

  /// constructor pero con parametro en JSON
  /// Item item = Item.fromJson(jsonData);
  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        description: json["description"],
        status: json["status"],
        price: json["price"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  /// conversion Item a formato json
  Map<String, dynamic> toJson() => {
        "id": id,
        "description": description,
        "status": status,
        "price": price,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
