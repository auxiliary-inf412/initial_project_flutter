import 'package:flutter/material.dart';
import 'package:initial_project_flutter/src/pages/camera_page.dart';
import 'package:initial_project_flutter/src/pages/ejemplo2_page.dart';
import 'package:initial_project_flutter/src/pages/home_page.dart';
import 'package:initial_project_flutter/src/pages/item_form_page.dart';
import 'package:initial_project_flutter/src/pages/item_list_page.dart';
import 'package:initial_project_flutter/src/pages/maps_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Proyecto para SI2',
      debugShowCheckedModeBanner: false,
      initialRoute: 'home_page',
      routes: {
        'home_page': (context) => const MyHomePage(),
        'ejemplo2_page': (context) => Ejemplo2Page(),
        'item_form_page': (context) => ItemFormPage(),
        'item_list_page': (context) => ItemListPage(),
        'maps_page': (context) => MapsPage(),
        'camera_page': (context) => CameraPage()
      },
    );
  }
}
